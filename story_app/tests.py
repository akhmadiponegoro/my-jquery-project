import time
import unittest
import os
from django.test import TestCase, Client
from django.contrib.staticfiles.testing import LiveServerTestCase
from django.core.management import call_command
from django.urls import reverse
from story_project.settings import BASE_DIR
from selenium import webdriver
from selenium.webdriver.common.keys import Keys 
from selenium.webdriver.chrome.options import Options


# Create your tests here.
class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(executable_path=os.path.join(BASE_DIR,"chromedriver"),chrome_options=chrome_options)
        super(FunctionalTest,self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest,self).tearDown()
    
    def test_ada_tombol_ganti_tema(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        tombol_tema = "switch"
        self.assertIn(tombol_tema,selenium.page_source)
        print("ada tombol ubah tema")

    def test_red_mode(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        tombol_tema = selenium.find_element_by_class_name("switch")
        tombol_tema.click()
        time.sleep(5)
        background_web =  selenium.find_element_by_tag_name("body").value_of_css_property("background-color")
        self.assertEquals('rgba(34, 32, 155, 1)', background_web)

    def test_blue_mode(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        tombol_tema = selenium.find_element_by_class_name("switch")
        tombol_tema.click()
        time.sleep(5)
        tombol_tema.click()
        time.sleep(5)
        background =  selenium.find_element_by_tag_name("body").value_of_css_property("background-color")
        self.assertEquals('rgba(182, 33, 33, 1)', background)
    
    def test_accordion_exist(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        accordion_ada = selenium.find_element_by_id("jQuery-accordion")
        self.assertIsNotNone(accordion_ada)
    
    def test_header_accordion_correct(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        accordion_header = selenium.find_element_by_name("accordion-header-1").text
        self.assertEqual("Aktivitas",accordion_header)

    def test_content_accordion_correct(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        accordion_1 = selenium.find_element_by_id('ui-id-1') 
        accordion_1.click()
        accordion_content = selenium.find_element_by_name('content-accordion-1').text
        self.assertIn("Vice Project Officer\nEDUCARE 2020",accordion_content)

    def test_accordion_closed(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        time.sleep(3)
        accordion = selenium.find_element_by_id("ui-id-2").value_of_css_property("display")
        self.assertEqual("none" , accordion)
    
    def test_accordion_opened(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        accordion_1 = selenium.find_element_by_id('ui-id-1') 
        accordion_1.click()
        time.sleep(3)
        accordion_content= selenium.find_element_by_id("ui-id-2").value_of_css_property("display")
        self.assertEqual("block" , accordion_content)

    def test_accordion_close_after_opened(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        accordion_1 = selenium.find_element_by_id('ui-id-1') 
        accordion_1.click()
        time.sleep(3)
        accordion_1.click()
        time.sleep(3)
        accordion_content= selenium.find_element_by_id("ui-id-2").value_of_css_property("display")
        self.assertEqual("none" , accordion_content)
        
class UnitTest(TestCase):

    def test_app_url_is_exist(self):
        self.client = Client()
        response = self.client.get('')
        self.assertEqual(response.status_code,200)
	
    def test_app_url_is_not_exist(self):
        self.client = Client()
        response = self.client.get('/none/')
        self.assertEqual(response.status_code,404)

    def test_app_template_is_correct (self):
        self.client = Client()
        response = self.client.get('')
        self.assertTemplateUsed(response, 'index.html')
        